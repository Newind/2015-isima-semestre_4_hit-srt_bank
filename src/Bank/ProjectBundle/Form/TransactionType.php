<?php

namespace Bank\ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TransactionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('transactionLabel', null, array('label' => 'Label :'))
            ->add('source', 'choice', array('choices' => $options['accounts'], 'label' => 'Source :'))
            ->add('destination', 'choice', array('choices' => $options['accounts'], 'label' => 'Destination :'))
            ->add('amount', null, array('label' => 'Amount :'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bank\ProjectBundle\Entity\Transaction'
        ));

        $resolver->setRequired(array(
            'accounts',
        ));

        $resolver->setDefaults(array(
            'accounts' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bank_projectbundle_transaction';
    }
}
